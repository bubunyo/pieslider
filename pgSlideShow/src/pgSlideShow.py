#!/usr/bin/env python
"""
A pygame program to show a slideshow of all images buried in a given directory.

Originally Released: 2007.10.31 (Happy halloween!)

"""
from collections import namedtuple
import os
import requests
from threading import Thread
import stat
import sys
import time

import signal

import pygame
from pygame.locals import QUIT, KEYDOWN, K_ESCAPE

file_list = []  # a list of all images being shown
title = "Smart Cart | Slideshow!"  # caption of the window...
waittime = 4  # default time to wait between images (in seconds)

pygame.init()
modes = max(pygame.display.list_modes())
# modes = (1024,768)
pygame.display.set_mode(modes)
# print modes
screen = pygame.display.get_surface()
(display_width, display_height) = screen.get_size()
centre = ((display_width / 2), (display_height / 2))

running = True
tag_read = False

# Create an object of the class MFRC522
# _base_url = 'http://192.168.43.162:5000/'
_base_url = 'http://127.0.0.1:5000/'


def walktree(top, callback):
    """recursively descend the directory tree rooted at top, calling the
    callback function for each regular file. Taken from the module-stat
    example at: http://docs.python.org/lib/module-stat.html
    """
    for f in os.listdir(top):
        pathname = os.path.join(top, f)
        mode = os.stat(pathname)[stat.ST_MODE]
        if stat.S_ISDIR(mode):
            # It's a directory, recurse into it
            walktree(pathname, callback)
        elif stat.S_ISREG(mode):
            # It's a file, call the callback function
            callback(pathname)
        else:
            # Unknown file type, print a message
            print 'Skipping %s' % pathname


def addtolist(file, extensions=['.png', '.jpg', '.jpeg', '.gif', '.bmp']):
    """Add a file to a global list of image files."""
    global file_list  # ugh
    filename, ext = os.path.splitext(file)
    e = ext.lower()
    # Only add common image types to the list.
    if e in extensions:
        print 'Adding to list: ', file
        file_list.append(file)
    else:
        print 'Skipping: ', file, ' (NOT a supported image)'


def input():
    while True:
        events = pygame.event.get()
        """A function to handle keyboard/mouse/device input events. """
        for event in events:  # Hit the ESC key to quit the slideshow.
            if (event.type == QUIT or
                    (event.type == KEYDOWN and event.key == K_ESCAPE)):
                # slide_show.
                global running
                running = False
                # if loading.is_alive:
                # loading.start()

        time.sleep(0.01)


def main(startdir="pics/"):
    print("slide show started")
    global file_list, title, waittime
    # Test for image support
    if not pygame.image.get_extended():
        print "Your Pygame isn't built with extended image support."
        print "It's likely this isn't going to work."
        sys.exit(1)

    walktree(startdir, addtolist)  # this may take a while...
    if len(file_list) == 0:
        print "Sorry. No images found. Exiting."
        sys.exit(1)
    pygame.display.set_caption(title)

    current = 0
    num_files = len(file_list)
    background = pygame.Surface(screen.get_size())
    background.fill((255, 255, 255))
    while True:
        try:
            img = pygame.image.load(file_list[current])
            img = img.convert()
            # rescale the image to fit the current display
            # img = pygame.transform.scale(img, max(modes))

            (width, height) = img.get_size()

            # print "width,height:", width, height
            if height > display_height:
                if height > width:
                    scaleratio = height / (display_height + 0.0)
                    newheight = display_height
                    newwidth = int(width / scaleratio)
                    # print "scaling to", newwidth, newheight
                else:
                    scaleratio = width / (display_width + 0.0)
                    newwidth = display_width
                    newheight = int(height / scaleratio)
                    # print "scaling to", newwidth, newheight

                scaled = pygame.transform.scale(img, (newwidth, newheight))
                scaled.set_colorkey()
            else:
                scaled = img
                newwidth = width
                newheight = height

            x = (display_width / 2) - (newwidth / 2)
            y = (display_height / 2) - (newheight / 2)

            screen.blit(background, (0, 0))
            screen.blit(scaled, (x, y))
            pygame.display.flip()
            time.sleep(waittime)
            if tag_read:
                raise Exception("Slide Show stopped")
                # print running;
        except pygame.error as err:
            print "Failed to display %s: %s" % (file_list[current], err)
            break


        # When we get to the end, re-start at the beginning
        current = (current + 1) % num_files


def split_screen():
    print("slit screen started")
    background = pygame.Surface(screen.get_size())
    background.fill((255, 255, 255))
    left = (0, 0, display_width / 2, display_height)
    right = (display_width / 2, 0, display_width / 2, display_height)
    screen.blit(screen, (0, 0), left)
    background.fill((255, 255, 255))
    screen.blit(background, (0, 0))
    walktree("pics/", addtolist)  # this may take a while...
    if len(file_list) == 0:
        print "Sorry. No images found. Exiting."
        sys.exit(1)
    pygame.display.set_caption(title)
    current = 0
    num_files = len(file_list)

    font = pygame.font.SysFont('Arial', 50)

    # using custom font
    # pygame.font.Font("path/to/font.ttf",size)

    product_name = font.render("Product Name", 1, (0, 0, 0))
    product_price = font.render("Price", 1, (0, 0, 0))
    product_description = font.render("description", 1, (0, 0, 0))
    product_promo = font.render("promo", 1, (0, 0, 0))

    json_obj = load_product_info("123")
    if json_obj is not None and json_obj.flag == 601:
        print(json_obj.flag)
        product = json_obj.data.product
        product_name = font.render(product.product_name, 1, (0, 0, 0))
        product_price = font.render(str(product.price), 1, (0, 0, 0))
        product_description = font.render(product.desc, 1, (0, 0, 0))
        product_promo = font.render(product.promo, 1, (0, 0, 0))

    screen.blit(product_name, (display_width / 2 + 20, 100))
    screen.blit(product_description, (display_width / 2 + 20, 200))
    screen.blit(product_price, (display_width / 2 + 20, 300))
    screen.blit(product_promo, (display_width / 2 + 20, 400))

    pygame.display.update(right)

    while True:
        if not tag_read:
            if not slide_show.is_alive:
                slide_show.start()
                print("slide show started again")
            raise Exception("Tag not read")
        img = pygame.image.load(file_list[current])
        img = img.convert()

        (width, height) = img.get_size()
        d_width = display_width / 2

        # print "width,height:", width, height
        if height > display_height:
            if height > width:
                scaleratio = height / (display_height + 0.0)
                newheight = display_height
                newwidth = int(width / scaleratio)
                # print "scaling to", newwidth, newheight
            else:
                scaleratio = width / (d_width + 0.0)
                newwidth = d_width
                newheight = int(height / scaleratio)
                # print "scaling to", newwidth, newheight

            scaled = pygame.transform.scale(img, (newwidth, newheight))
            scaled.set_colorkey()
        else:
            scaled = img
            newwidth = width
            newheight = height

        x = (d_width / 2) - (newwidth / 2)
        y = (display_height / 2) - (newheight / 2)

        screen.blit(background, (0, 0))
        screen.blit(scaled, (x, y))
        pygame.display.update(left)
        time.sleep(2)
        current = (current + 1) % num_files


def end_read(signal, frame):
    import RPi.GPIO as GPIO

    global continue_reading
    print "Ctrl+C captured, ending read."
    continue_reading = False
    GPIO.cleanup()


signal.signal(signal.SIGINT, end_read)


def read_tag():
    return "123456"
    import MFRC522

    MIFAREReader = MFRC522.MFRC522()

    # Scan for cards
    (status, TagType) = MIFAREReader.MFRC522_Request(MIFAREReader.PICC_REQIDL)

    # If a card is found
    if status == MIFAREReader.MI_OK:
        print "Card detected"

    # Get the UID of the card
    (status, uid) = MIFAREReader.MFRC522_Anticoll()

    # If we have the UID, continue
    if status == MIFAREReader.MI_OK:

        # Print UID
        # print "Card read UID: " + str(uid[0]) + "," + str(uid[1]) + "," + str(uid[2]) + "," + str(uid[3])
        tag_id = str(uid[0]) + str(uid[1]) + str(uid[2]) + str(uid[3])
        print "Card read UID: " + tag_id
        print(type(tag_id))

        # This is the default key for authentication
        key = [0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF]

        # Select the scanned tag
        MIFAREReader.MFRC522_SelectTag(uid)

        # Authenticate
        status = MIFAREReader.MFRC522_Auth(MIFAREReader.PICC_AUTHENT1A, 8, key, uid)

        # Check if authenticated
        if status == MIFAREReader.MI_OK:
            MIFAREReader.MFRC522_Read(8)
            MIFAREReader.MFRC522_StopCrypto1()
        else:
            print "Authentication error"

        # print(type(uid))

        return tag_id
    else:
        return None


def display_loading_gif():
    angle = 0;
    img = pygame.image.load("./assets/in.png")
    background = pygame.Surface(screen.get_size())
    # print(background)
    # print(centre)
    (display_width, display_height) = screen.get_size()
    img_c = img.convert()
    (width, height) = img_c.get_size()
    x = (display_width / 2) - (width / 2)
    y = (display_height / 2) - (height / 2)
    print x, y
    background.fill((255, 255, 255))
    screen.blit(background, (0, 0))
    try:

        screen.blit(img, centre)
        while True:
            out = pygame.image.load("./assets/out.png")
            angle += 15
            # ensure angle does not increase indefinitely
            angle %= 360
            # create a new, rotated Surface
            surf = rot_center(out, angle)
            screen.blit(background, (0, 0))
            screen.blit(img, (x, y))
            screen.blit(surf, (x, y))
            pygame.display.flip()
            time.sleep(0.01)

    except Exception as e:
        print(e)


def rot_center(image, angle):
    """rotate an image while keeping its center and size"""
    orig_rect = image.get_rect()
    rot_image = pygame.transform.rotate(image, angle)
    rot_rect = orig_rect.copy()
    rot_rect.center = rot_image.get_rect().center
    rot_image = rot_image.subsurface(rot_rect).copy()
    return rot_image


def load_product_info(t):
    try:
        rfid = str(t)
        r = requests.get(_base_url + 'product/rfid/' + rfid)
        return get_json_obj(r.text)
        # json_data = r.text
        # json_obj =
        # Logger.info('Flag: ' + str(json_obj.flag))
        # return json_obj
    except Exception as e:
        print(e)

        return None


def get_json_obj(data):
    return json2obj(data)


def _json_object_hook(d):
    return namedtuple('X', d.keys())(*d.values())


def json2obj(data):
    import json

    return json.loads(data, object_hook=_json_object_hook)


def tag_reading_event():
    # global tag_read
    # a = 0
    # while True:
    # if a % 5 is 1:
    # print("in")
    # uid = read_tag()
    # if uid is not None:
    # tag_read = True
    # split.start()
    # else:
    # tag_read = False
    #             slide_show.start()
    #     else:
    #         tag_read = False
    #         print("out")
    #     time.sleep(1)
    #     a += 1
    pass


slide_show = Thread(target=main)
key_event = Thread(target=input)
# loading = Thread(target=display_loading_gif)
split = Thread(target=split_screen)
reader = Thread(target=tag_reading_event)

if __name__ == '__main__':
    a = 0
    slide_show.start()
    while True:
        if a % 5 is 1:
            print("in")
            uid = read_tag()
            if uid is not None:
                tag_read = True
                split.start()
            else:
                tag_read = False

        else:
            tag_read = False
            print("out")
        time.sleep(1)
        a += 1

        # main()
        # tag_reading_event()

        # key_event.start()
        # slide_show.join()
        # key_event.join()
        # split.start()
        # split.join()
