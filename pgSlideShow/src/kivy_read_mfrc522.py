#!/usr/bin/env python
# -*- coding: utf8 -*-

import RPi.GPIO as GPIO
from kivy.uix.label import Label
import MFRC522
from kivy.app import App
from kivy.clock import Clock
from kivy.uix.boxlayout import BoxLayout
import signal

continue_reading = True
# Hook the SIGINT

def end_read(signal, frame):
    global continue_reading
    print "Ctrl+C captured, ending read."
    continue_reading = False
    GPIO.cleanup()


class RootWidget(BoxLayout):
    def __init__(self, **kwargs):
        super(RootWidget, self).__init__(**kwargs)
        l = Label(text="sample uid")
        self.add_widget(l)
        signal.signal(signal.SIGINT, end_read)
        # Create an object of the class MFRC522
        self.MIFAREReader = MFRC522.MFRC522()
        Clock.schedule_interval(self.readCard, 3.0)

    def readCard(self):
        # Scan for cards
        (status, TagType) = self.MIFAREReader.MFRC522_Request(self.MIFAREReader.PICC_REQIDL)

        # If a card is found
        if status == self.MIFAREReader.MI_OK:
            print "Card detected"

        # Get the UID of the card
        (status, uid) = self.MIFAREReader.MFRC522_Anticoll()

        # If we have the UID, continue
        if status == self.MIFAREReader.MI_OK:

            # Print UID
            # print "Card read UID: " + str(uid[0]) + "," + str(uid[1]) + "," + str(uid[2]) + "," + str(uid[3])
            uid = str(uid[0]) + str(uid[1]) + str(uid[2]) + str(uid[3])
            print "Card read UID: " + uid

            self.add_widget(Label(text=uid))

            # This is the default key for authentication
            key = [0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF]

            # Select the scanned tag
            self.MIFAREReader.MFRC522_SelectTag(uid)

            # Authenticate
            status = self.MIFAREReader.MFRC522_Auth(self.MIFAREReader.PICC_AUTHENT1A, 8, key, uid)

            # Check if authenticated
            if status == self.MIFAREReader.MI_OK:
                self.MIFAREReader.MFRC522_Read(8)
                self.MIFAREReader.MFRC522_StopCrypto1()
            else:
                print "Authentication error"


class KivyRead(App):
    def build(self):
        return RootWidget()


if __name__ == '__main__':
    KivyRead().run()