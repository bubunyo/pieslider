import os, time
from random import randint
import threading
from collections import namedtuple
import os
import requests
import stat
import sys
import time
import MFRC522

# import signal

import pygame
import io
from urllib2 import urlopen


title = "Smart Cart | Slideshow!"  # caption of the window...
waittime = 4  # default time to wait between images (in seconds)
tag_id = None

pygame.init()
modes = max(pygame.display.list_modes())
# modes = (1024,768)
pygame.display.set_mode(modes)
# print modes
screen = pygame.display.get_surface()
(display_width, display_height) = screen.get_size()
centre = ((display_width / 2), (display_height / 2))

# Create an object of the class MFRC522
# _base_url = 'http://192.168.43.162:5000/'
_base_url = 'http://192.168.43.43:5000/'
# _base_url = 'http://127.0.0.1:5000/'


def walktree(top):
    file_list = []
    for f in os.listdir(top):
        pathname = os.path.join(top, f)
        mode = os.stat(pathname)[stat.ST_MODE]
        if stat.S_ISREG(mode):
            # It's a file, call the callback function
            file_list = addtolist(file_list, pathname)
        else:
            # Unknown file type, print a message
            print 'Skipping %s' % pathname
    return file_list


def addtolist(file_list, file, extensions=['.png', '.jpg', '.jpeg', '.gif', '.bmp']):
    filename, ext = os.path.splitext(file)
    e = ext.lower()
    # Only add common image types to the list.
    if e in extensions:
        # print 'Adding to list: ', file
        file_list.append(file)
    else:
        print 'Skipping: ', file, ' (NOT a supported image)'
    return file_list


class SlideShowThread(threading.Thread):
    # Ask the thread to stop by calling its join() method.
    def __init__(self):
        super(SlideShowThread, self).__init__()
        self.stoprequest = threading.Event()
        self.file_list = []  # a list of all images being shown
        self.start()

    def run(self):
        startdir = "pics/"
        print("Slide Show running")
        global title, waittime
        # Test for image support
        if not pygame.image.get_extended():
            print "Your Pygame isn't built with extended image support."
            print "It's likely this isn't going to work."
            sys.exit(1)

        self.file_list = walktree(startdir)  # this may take a while...
        if len(self.file_list) == 0:
            print "Sorry. No images found. Exiting."
            sys.exit(1)

        current = 0
        num_files = len(self.file_list)
        background = pygame.Surface(screen.get_size())
        background.fill((255, 255, 255))
        self.stoprequest.clear()
        while not self.stoprequest.isSet():
            try:
                img = pygame.image.load(self.file_list[current])
                img = img.convert()
                # rescale the image to fit the current display
                # img = pygame.transform.scale(img, max(modes))

                (width, height) = img.get_size()

                # print "width,height:", width, height
                if height > display_height:
                    if height > width:
                        scaleratio = height / (display_height + 0.0)
                        newheight = display_height
                        newwidth = int(width / scaleratio)
                        # print "scaling to", newwidth, newheight
                    else:
                        scaleratio = width / (display_width + 0.0)
                        newwidth = display_width
                        newheight = int(height / scaleratio)
                        # print "scaling to", newwidth, newheight

                    scaled = pygame.transform.scale(img, (newwidth, newheight))
                    scaled.set_colorkey()
                else:
                    scaled = img
                    newwidth = width
                    newheight = height

                x = (display_width / 2) - (newwidth / 2)
                y = (display_height / 2) - (newheight / 2)

                screen.blit(background, (0, 0))
                screen.blit(scaled, (x, y))
                pygame.display.flip()
                time.sleep(waittime)

            except Exception as err:
                print(err)
            # When we get to the end, re-start at the beginning
            current = (current + 1) % num_files

    def join(self, timeout=None):
        print("stoping the slide show")
        self.stoprequest.set()
        super(SlideShowThread, self).join(timeout)


class SplitScreenThread(threading.Thread):
    def __init__(self):
        super(SplitScreenThread, self).__init__()
        self.stoprequest = threading.Event()
        self.file_list = []
        self.start()

    def run(self):
        font = pygame.font.SysFont('Arial', 50)
        json_obj = self.load_product_info(tag_id)

        if json_obj is not None and json_obj.flag == 601:
            pass
        else:
            background = pygame.Surface(screen.get_size())
            background.fill((255, 255, 255))
            img = pygame.image.load('notfound/stop.png')
            img = img.convert()
            # detection_mode = font.render("System Maintenace going on", 1, (0, 0, 0))
            screen.blit(background, (0, 0))
            screen.blit(img, (0, 0))
            # screen.blit(detection_mode, (display_width - 200 + 20, 700))
            pygame.display.update((0, 0, display_width, display_height))
            time.sleep(5)
            raise Exception("Sys Maintenance")

        print("split screen started")
        background = pygame.Surface(screen.get_size())
        background.fill((255, 255, 255))
        left = (0, 0, display_width / 2, display_height)
        right = (display_width / 2, 0, display_width / 2, display_height)
        screen.blit(screen, (0, 0), left)
        background.fill((255, 255, 255))
        screen.blit(background, (0, 0))
        self.file_list = walktree("pics/")  # this may take a while...
        if len(self.file_list) == 0:
            print "Sorry. No images found. Exiting."
            sys.exit(1)
        current = 0
        num_files = len(self.file_list)


        # using custom font
        # pygame.font.Font("path/to/font.ttf",size)

        product_name = font.render("Detection mode", 1, (0, 0, 0))
        product_price = font.render("Detection mode", 1, (0, 0, 0))
        product_description = font.render("Detection mode", 1, (0, 0, 0))
        product_promo = font.render("Detection mode", 1, (0, 0, 0))

        # rfid = '123'
        # rfid = tag_id


        try:
            if json_obj is not None and json_obj.flag == 601:
                catalog = json_obj.data
                product = catalog.product

                product_name = font.render(product.product_name, 1, (0, 0, 0))
                product_price = font.render(str(product.price), 1, (0, 0, 0))
                product_description = font.render(product.info, 1, (0, 0, 0))
                product_promo = font.render(product.promo, 1, (0, 0, 0))
                # for url in catalog.urls:
                # print(len(catalog.urls))
        except:
            pass

        screen.blit(product_name, (display_width / 2 + 20, 100))
        screen.blit(product_description, (display_width / 2 + 20, 200))
        screen.blit(product_price, (display_width / 2 + 20, 300))
        screen.blit(product_promo, (display_width / 2 + 20, 400))

        pygame.display.update(right)

        self.stoprequest.clear()
        num = len(catalog.urls)
        next_img = 0

        while not self.stoprequest.isSet():

            # init images here

            image_url = catalog.host + catalog.urls[next_img]
            next_img = (next_img + 1) % num
            image_str = urlopen(image_url).read()
            image_file = io.BytesIO(image_str)
            if json_obj.flag != 601:
                image_file = 'notfound/stop.png'
            img = pygame.image.load(image_file)
            img = img.convert()

            (width, height) = img.get_size()
            d_width = display_width / 2

            # print "width,height:", width, height
            if height > display_height:
                if height > width:
                    scaleratio = height / (display_height + 0.0)
                    newheight = display_height
                    newwidth = int(width / scaleratio)
                    # print "scaling to", newwidth, newheight
                else:
                    scaleratio = width / (d_width + 0.0)
                    newwidth = d_width
                    newheight = int(height / scaleratio)
                    # print "scaling to", newwidth, newheight

                scaled = pygame.transform.scale(img, (newwidth, newheight))
                scaled.set_colorkey()
            else:
                scaled = img
                newwidth = width
                newheight = height

            x = (d_width / 2) - (newwidth / 2)
            y = (display_height / 2) - (newheight / 2)

            screen.blit(background, (0, 0))
            screen.blit(scaled, (x, y))
            pygame.display.update(left)
            time.sleep(2)
            # current = (current + 1) % num_files

    def join(self, timeout=None):
        self.stoprequest.set()
        super(SplitScreenThread, self).join(timeout)

    def _files_in_dir(self, dirname):
        """ Given a directory name, yields the names of all files (not dirs)
            contained in this directory and its sub-directories.
        """
        for path, dirs, files in os.walk(dirname):
            for file in files:
                yield os.path.join(path, file)

    def load_product_info(self, t):
        try:
            rfid = str(t)
            r = requests.get(_base_url + 'product/rfid/' + rfid)
            return self.get_json_obj(r.text)
        except Exception as e:
            print(e)

            return None


    def get_json_obj(self, data):
        return self.json2obj(data)


    def _json_object_hook(self, d):
        return namedtuple('X', d.keys())(*d.values())


    def json2obj(self, data):
        import json

        return json.loads(data, object_hook=self._json_object_hook)


class TagReaderThread(threading.Thread):
    def __init__(self):
        super(TagReaderThread, self).__init__()
        self.stoprequest = threading.Event()
        self.MIFAREReader = MFRC522.MFRC522()
        self.start()

    def run(se

        global tag_id

        self.stoprequest.clear()
        while not self.stoprequest.isSet():
            # tag_id = '123'
            tag_id = self.read_tag()
            # print(tag_id)

    def read_tag(self):

        # Scan for cards
        (status, TagType) = self.MIFAREReader.MFRC522_Request(self.MIFAREReader.PICC_REQIDL)

        # If a card is found
        if status == self.MIFAREReader.MI_OK:
            print "Card detected"

        # Get the UID of the card
        (status, uid) = self.MIFAREReader.MFRC522_Anticoll()

        # If we have the UID, continue
        if status == self.MIFAREReader.MI_OK:
            tag_id = str(uid[0]) + str(uid[1]) + str(uid[2]) + str(uid[3])
            print "Card read UID: " + tag_id
            # print(type(tag_id))

            return tag_id
        else:
            return None

    def join(self, timeout=None):
        self.stoprequest.set()
        super(TagReaderThread, self).join(timeout)
        self.end_read()

    def end_read(self):
        # Ending tag read and cleaning up of gpio pins
        import RPi.GPIO as GPIO

        GPIO.cleanup()


class EventSubSystem(threading.Thread):
    def __init__(self):
        super(EventSubSystem, self).__init__()
        self.start()

    def run(self):
        while True:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    raise SystemExit


def main():
    slide_show = SlideShowThread()
    TagReaderThread()
    # EventSubSystem()
    split = None
    while True:
        # time.sleep(1)
        if tag_id is not None:
            # if True:
            if slide_show.isAlive():
                slide_show.join()
                split = SplitScreenThread()
        else:
            if split is not None:
                split.join()
                split = None
                slide_show = SlideShowThread()


if __name__ == '__main__':
    main()